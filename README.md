# airhorn-cmds
> An NPM Package to make creating new Discord.JS bots efficiently

[![NPM Version][npm-image]][npm-url]
[![Downloads Stats][npm-downloads]][npm-url]
[![Discord Stats][discord-image]][discord-url]

Airhorn-cmds currently includes a command handler for your Discord.JS bots.


## Installation

```sh
npm install airhorn-cmds --save
```

## Setup guide
1 - To start using the Command Handler after installation, we'll first need require airhorn-cmds and create a new Command Handler with the proper folder name and prefixes.

```js
const { CommandHandler } = require("airhorn-cmds")
const CH = new CommandHandler({
    folder: __dirname + '/commands/',
    prefix: ['?', '!', '+', 'mygigabot']
  });
```

2 - Inside of the message event, we're going to do a little parsing and checking if they ran an available command or not.

```js
bot.on("message", (message) => {
    if(message.channel.type === 'dm') return;
    if(message.author.type === 'bot') return;
    let args = message.content.split(" ");
    let command = args[0];
    let cmd = CH.getCommand(command);
    if(!cmd) return;

    try{
        cmd.run(bot,message,args)
    }catch(e){
        console.log(e)
    }

});
```

3 - And of course we're going to need a command file. So inside of your bot folder, create a folder called commands. I'm going to create a file called
test.js and put the following code inside of it.

```js
module.exports = class test {
    constructor(){
            this.name = 'test',
            this.alias = ['t'],
            this.usage = '?test'
    }

    async run(bot, message, args) {
        await message.delete();
        message.reply(this.name + " worked!")
    }
}
```